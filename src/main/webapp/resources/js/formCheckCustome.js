function text(name) {
    document.getElementById(name).addEventListener('keypress', function (e) {

        if ((e.charCode >= 97 && e.charCode <= 122) || (e.charCode >= 65 && e.charCode <= 90)) {
            alert("لطفا فقط از حروف فارسی استفاده کنید");
            e.preventDefault();
        }


    });

}

function validateCustomer(formID, action) {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName(formID);
    y = document.forms[formID].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid && action == "submitForm") {
        document.getElementById(formID).submit();
    } else if (valid && action == "addCondition") {
       var condition=document.getElementById("conditionMaker");
    x.append(condition);
    }
    return valid; // return the valid status
}
