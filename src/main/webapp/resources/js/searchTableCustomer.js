function submitForm(form){
    document.getElementById("results").innerHTML="";
    var url = "/customer/customerService.do?action=search";
    var formData = $(form).serializeArray();
    $.post(url, formData).done(function (data) {
if(data.length>0) {
    var x;
    var $table = $('    <table class="table table-striped table-bordered table-hover dataTable no-footer"\n' +
        '           id="dataTables-search" role="grid"\n' +
        '           aria-describedby="dataTables-example_info">\n' +
        '        <thead>\n' +
        '        <tr role="row">\n' +
        '            <th class="sorting_asc" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-sort="ascending"\n' +
        '                aria-label="Rendering engine: activate to sort column descending">\n' +
        '                شماره مشتری\n' +
        '            <th class="sorting_asc" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-sort="ascending"\n' +
        '                aria-label="Rendering engine: activate to sort column descending">\n' +
        '                نام\n' +
        '            </th>\n' +
        '            <th class="sorting" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-label="Browser: activate to sort column ascending">\n' +
        '                نام خانوادگی\n' +
        '            </th>\n' +
        '            <th class="sorting" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-label="Platform(s): activate to sort column ascending">\n' +
        '                نام پدر\n' +
        '            </th>\n' +
        '            <th class="sorting" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-label="Engine version: activate to sort column ascending">\n' +
        '                شماره ملی\n' +
        '            </th>\n' +
        '            <th class="sorting" tabindex="0"\n' +
        '                aria-controls="dataTables-example" rowspan="1"\n' +
        '                colspan="1" style="width: auto;"\n' +
        '                aria-label="CSS grade: activate to sort column ascending">\n' +
        '                تاریخ تولد\n' +
        '            </th>\n' +
        '        </tr>\n' +
        '        </thead>\n' +
        '        <tbody>').appendTo($('#results'));
    for (var i = 0; i < data.length; i++) {
        x = data[i];


        var $tr = $('<tr class="gradeA odd" role="row">').appendTo($table);
        $('<td class="sorting_1">').text(x.id).appendTo($tr);
        $('<td class="sorting_1">').text(x.name).appendTo($tr);

        $('<td   class="center">').text(x.family).appendTo($tr);

        $('<td   class="center">').text(x.fatherName).appendTo($tr);

        $('<td   class="center">').text(x.nationalCod).appendTo($tr);

        $('<td   class="center">').text(x.birthDay).appendTo($tr);

    }
$endBody=$('</body></table>').appendTo($table)
}
else
    var $notFound=$('<div>داده ای با مشخصات درخواستی یافت نشد </div>').appendTo($('#results'));
    });
}
