<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.entity.Customer" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="fa">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    <!-- Bootstrap Core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/resources/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="/resources/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/resources/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/resources/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/resources/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/resources/css/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link href="/resources/css/popUp.css" rel="stylesheet">
    <link href="/resources/css/custom.css" rel="stylesheet">
    <link href="/resources/css/formMulti.css" rel="stylesheet">
    <link href="/resources/css/calendar-blue.css" rel="stylesheet">
    <%@include file="menu.jsp" %>
</head>

<body>

<div id="wrapper">
    <div id="page-wrapper" style="min-height: 334px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">مدیریت کاربران حقوقی</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        پنل مدیریت
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="${activeBaseLI}">
                                <a href="#home" data-toggle="tab"><i class="fa fa-certificate" aria-hidden="true">
                                    معرفی </i></a></li>
                            <li class="${activeRegisterLI}">

                                <a href="#profile" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"> ثبت کاربر
                                    جدید </i></a></li>
                            <li class="${activeUpdateLI}">
                                <a href="#messages" data-toggle="tab"><i class="fa fa-list" aria-hidden="true"> اصلاح و
                                    حذف اطلاعات </i></a></li>
                            <li class="${activeSearchLI}"><a href="#search" data-toggle="tab"><i class="fa fa-search"
                                                                                                 aria-hidden="true">
                                جستجو پیشرفته </i></a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade ${activeBasePanel}" id="home">
                                <h4>معرفی</h4>
                                <p> دومین نوع از انواع شخصیت در حقوق ، شخصیت حقوقی است . فرض کنید تعدادی از افراد با یک
                                    هدف
                                    مشخص اقدام به تاسیس یک شرکت یا موسسه می کنند . مثلا یک شرکت تضامنی یا یک شرکت
                                    مسئولیت
                                    محدود یا یک موسسه انتفاعی . این شرکت یا موسسه ایجاد شده ، انسان نیستند تا بتوانند
                                    صاحب
                                    حق و حقوق شوند و مانند انسان ها وجود طبیعی هم ندارند اما از نظر اعتباری دارای شخصیت
                                    هستند . یعنی حقوق آنها را شایسته داشتن یک سری حقوق و تکالیف می داند ، بنابراین می
                                    توانند
                                    دارای شخصیت باشند که به آن ، شخصیت حقوقی می گویند .</p>
                            </div>

                            <div class="tab-pane fade ${activeRegisterPanel}" id="profile">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="signup-content">
                                            <div class="signup-form">
                                                <h2 class="form-title">ثبت اطلاعات</h2>
                                                <form id="regForm" method="POST"
                                                      action="/legalCustomer/FinancialFacilitiesService.do?action=save">
                                                    <div class="tab"> معرفی تسهیلات :
                                                        <div class="form-group">
                                                            <label for="typeFacilities">نام نوع تسهیلات : </label>
                                                            <input id="typeFacilities" name="typeFacilities" type="text"
                                                                   placeholder="نام نوع تسهیلات...">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="interestRate">نارخ سود : </label>
                                                            <input id="interestRate" name="interestRate" type="text"
                                                                   placeholder="نرخ سود...">
                                                        </div>
                                                    </div>
                                                    <div class="tab">اضافه کردن شروط :
                                                        <div id="entry1" class="clonedInput">
                                                            <h2 id="reference" name="reference"
                                                                class="heading-reference">شرط شماره 1</h2>
                                                            <fieldset>
                                                                <!-- Text input-->
                                                                <div class="form-group">
                                                                    <label class="label_nameCondition control-label"
                                                                           for="nameCondition">نام شرط</label>
                                                                    <input id="nameCondition" name="nameCondition"
                                                                           type="text" placeholder="نام شرط"
                                                                           class="input_nameCondition form-control"
                                                                           required="">
                                                                </div>

                                                                <!-- Text input-->
                                                                <div class="form-group">
                                                                    <label class="label_minimumTime control-label"
                                                                           for="minimumTime">حداقل مدت قرارداد
                                                                        : </label>
                                                                    <input id="minimumTime" name="minimumTime"
                                                                           type="text" placeholder=""
                                                                           class="input_minimumTime form-control">
                                                                </div>

                                                                <!-- Text input-->
                                                                <div class="form-group">
                                                                    <label class="label_maximumTime control-label"
                                                                           for="maximumTime">حداکثر مدت قرارداد
                                                                        : </label>
                                                                    <input id="maximumTime" name="maximumTime"
                                                                           type="text" placeholder=""
                                                                           class="input_maximumTime form-control">
                                                                </div>
                                                                <!-- Text input-->
                                                                <div class="form-group">
                                                                    <label class="label_minimumAmount control-label"
                                                                           for="minimumAmount">حداقل مبلغ قرارداد
                                                                        : </label>
                                                                    <input id="minimumAmount" name="minimumAmount"
                                                                           type="text" placeholder=""
                                                                           class="input_minimumAmount form-control">
                                                                </div>
                                                                <!-- Text input-->
                                                                <div class="form-group">
                                                                    <label class="label_maximumAmount control-label"
                                                                           for="maximumAmount">حداکثر مبلغ قرارداد
                                                                        : </label>
                                                                    <input id="maximumAmount" name="maximumAmount"
                                                                           type="text" placeholder=""
                                                                           class="input_maximumAmount form-control">
                                                                </div>
                                                            </fieldset>
                                                        </div>

                                                        <!-- Button (Double) -->
                                                        <div class="mb-3 text-right">
                                                            <button type="button" id="btnAdd" name="btnAdd"
                                                                    class="btn btn-outline-primary">اضافه کردن شرط جدید
                                                            </button>
                                                            <button type="button" id="btnDel" name="btnDel"
                                                                    class="btn btn-outline-danger">حذف اخرین شرط
                                                            </button>

                                                        </div>


                                                    </div> <!-- end wrapper -->

                                                    <div style="overflow:auto;">
                                                        <div style="float:right;">
                                                            <button type="button" id="prevBtn" onclick="nextPrev(-1)">
                                                                قبلی
                                                            </button>
                                                            <button type="button" id="nextBtn" onclick="nextPrev(1)">
                                                                بعدی
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <!-- Circles which indicates the steps of the form: -->
                                                    <div style="text-align:center;margin-top:40px;">
                                                        <span class="step"></span>
                                                        <span class="step"></span>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label id="response"
                                                               class="form-submit"><%=request.getAttribute("reponseRegister")%>
                                                        </label>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="signup-image">
                                                <figure><img alt=""
                                                             src="<%=request.getContextPath()%>/resources/images/signup-image.jpg">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade ${activeUpdatePanel}" id="messages">
                                <h4>به روز رسانی اطلاعات</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-striped table-bordered table-hover dataTable no-footer"
                                               id="dataTables-example" role="grid"
                                               aria-describedby="dataTables-example_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width:auto;"
                                                    aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    شماره مشتری
                                                </th>
                                                <th class="sorting_asc" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width:auto;"
                                                    aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    نام شرکت
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Browser: activate to sort column ascending">
                                                    تاریخ ثبت
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    کد اقتصادی
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    اطلاعات نماینده
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: 90px;"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    عملیات
                                                </th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            <c:forEach items="${requestScope.list}" var="p">
                                                <form method="post"
                                                      action="/legalCustomer/legalCustomerService.do?action=delete">
                                                    <input type="hidden" name="id" value="${p.id}"/>
                                                    <tr class="gradeA odd" role="row">
                                                        <td class="sorting_1">${p.id}</td>
                                                        <td class="sorting_1">${p.companyName}</td>
                                                        <td class="center"> ${p.registerDate}</td>
                                                        <td class="center">${p.economicCod}</td>
                                                        <td class="center"><a
                                                                href="javascript:openBox('${p.customer.id}','${p.customer.name}','${p.customer.family}','${p.customer.fatherName}','${p.customer.nationalCod}','${p.customer.birthDay}')"> ${p.customer.name} ${p.customer.family}</a>
                                                        </td>
                                                        <td class="center">

                                                            <button class="btn btn-danger"
                                                                    onclick="removePerson(${p.id},${p.customer.id})"><i
                                                                    class="fa fa-trash"></i></button>
                                                            <button class="btn btn-success" id="myBtn1"
                                                                    onclick="openBoxCompany('${p.id}','${p.companyName}','${p.registerDate}','${p.economicCod}')"
                                                                    type="button"><i class="fa fa-refresh"></i>
                                                            </button>

                                                        </td>

                                                    </tr>
                                                </form>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <!-- The Modal -->
                                        <div id="myModal" class="modal">

                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <span class="close">&times;</span>
                                                <div class="modal-body">
                                                    <form role="form" method="post" id="updateLegalCustomer"
                                                          action="/legalCustomer/legalCustomerService.do?action=update">
                                                        <input type="hidden" id="uid" name="id"/>
                                                        <p> فرم به روز رسانی اطلاعات </p>
                                                        <div class="form-group">
                                                            <label for="ucompanyName">نام شرکت :</label>
                                                            <input type="text" class="form-control" id="ucompanyName"
                                                                   name="companyName" placeholder=" نام شرکت... "
                                                                   required="" onkeypress="text('ucompanyName')"
                                                                   oninput="this.className = ''"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="uregisterDate"> تاریخ ثبت شرکت</label>
                                                            <input type="text" class="form-control" id="uregisterDate"
                                                                   name="registerDate" placeholder=" تاریخ ثبت شرکت... "
                                                                   required="" maxlength="50" onkeydown="return false;"
                                                                   onselect="dateJalali('uregisterDate')"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ueconomicCod"> کد اقتصادی:</label>
                                                            <input type="text" class="form-control"
                                                                   name="economicCod" id="ueconomicCod"
                                                                   placeholder=" کد اقتصادی... "
                                                                   onkeypress="text('ueconomicCod')"
                                                                   oninput="this.className = ''"/>
                                                        </div>

                                                        <div class="form-group form-button">

                                                            <button type="button"
                                                                    onclick="validateCustomer('updateLegalCustomer')">
                                                                به روز رسانی
                                                            </button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="customerModal" class="modal">

                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <span class="close">&times;</span>
                                                <div class="modal-body">
                                                    <h4> اطلاعات نماینده حساب</h4>
                                                    <form method="post"
                                                          action="/customer/customerService.do?action=update"
                                                          id="update-form">

                                                        <input type="hidden" id="uid1" name="id"/>
                                                        <input type="hidden" id="url" name="url"
                                                               value="/legalCustomer/legalCustomerService.do?action=findAll"/>
                                                        <div class="form-group">
                                                            <label for="name">نام</label>
                                                            <input type="text" oninput="this.className = ''" name="name"
                                                                   id="uname" placeholder="نام"
                                                                   onkeypress="text('uname')"/>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="birthDay">تاریخ تولد</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="birthDay"
                                                                   id="ubirthDay" placeholder="تاریخ تولد"
                                                                   onselect="dateJalali('ubirthDay')"
                                                                   onkeydown="return false;"
                                                                   oninput="this.className = ''"/>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="family">نام خانوادگی</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="family"
                                                                   id="ufamily" placeholder="نام خانوادگی"
                                                                   onkeypress="text('ufamily')"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="nationalCod">کدملی</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="nationalCod"
                                                                   id="unationalCod" placeholder="کدملی"
                                                                   onkeypress="text('unationalCod')"/>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fatherName">نام پدر</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="fatherName"
                                                                   id="ufatherName" placeholder="نام پدر"
                                                                   onkeypress="text('ufatherName')"/>
                                                        </div>
                                                        <div class="form-group form-button">

                                                            <button type="button"
                                                                    onclick="validateCustomer('update-form')">
                                                                به روز رسانی
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade ${activeSearchPanel}" id="search">
                                <h4>جستجو پیشرفته</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <form method="post"
                                              action="/legalCustomer/legalCustomerService.do?action=search">
                                            <div class="divRow">
                                                <div class="divCell"><label> و </label><input type="radio"
                                                                                              name="conditionCompanyName"
                                                                                              value="and"/></div>
                                                <div class="divCell"><label> یا </label><input type="radio"
                                                                                               name="conditionCompanyName"
                                                                                               value="or"/></div>
                                                <div class="divCell-text">نام شرکت</div>

                                                <div class="divCell"><input type="text" class="form-control"
                                                                            id="companyNameSearch" name="companyName"
                                                                            placeholder="نام شرکت..."
                                                                            onkeypress="text('companyNameSearch')"/>
                                                </div>
                                            </div>
                                            <div class="divRow">
                                                <div class="divCell"><label> و </label><input type="radio"
                                                                                              name="conditionEconomicCod"
                                                                                              value="and"/></div>
                                                <div class="divCell"><label> یا </label><input type="radio"
                                                                                               name="conditionEconomicCod"
                                                                                               value="or"/></div>
                                                <div class="divCell-text"> کداقتصادی</div>
                                                <div class="divCell"><input type="text" class="form-control"
                                                                            name="economicCod"
                                                                            placeholder="کداقتصادی..."/>
                                                </div>

                                            </div>
                                            <div class="divRow">
                                                <div class="divCell"><label> و </label><input type="radio"
                                                                                              name="conditionRegisterDate"
                                                                                              value="and"/></div>
                                                <div class="divCell"><label> یا </label><input type="radio"
                                                                                               name="conditionRegisterDate"
                                                                                               value="or"/></div>
                                                <div class="divCell-text">تاریخ ثبت شرکت</div>
                                                <div class="divCell">
                                                    <input placeholder="تاریخ ثبت شرکت..." class="form-control"
                                                           id="registerDate1" name="registerDate" type="text"
                                                           onkeydown="return false;"
                                                           onselect="dateJalali('registerDate1')">

                                                </div>

                                            </div>
                                            <div class="divRow">
                                                <div class="divCell">
                                                    <input type="button" value="جستجو" onclick="submitForm(this.form)">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div id="results">

                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>
<!-- /#wrapper -->


<!-- jQuery Version 1.11.0 -->
<script src="/resources/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/resources/js/metisMenu/metisMenu.min.js"></script>


<script src="/resources/js/jquery/jquery.dataTables.min.js"></script>
<script src="/resources/js/bootstrap/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/resources/js/sb-admin-2.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });

</script>
<script>
    function removePerson(legaCustomerlID, customerID) {
        if (confirm('ایا برای حذف اطمینان کافی دارید؟'))
            window.location = "/legalCustomer/legalCustomerService.do?action=delete&legalCustomerID=" + legaCustomerlID + "&customerID=" + customerID;
    }
</script>
<script src="/resources/js/popUp.js"></script>
<script src="/resources/js/searchTableLegalCustomer.js"></script>
<script src="/resources/js/formMulti.js"></script>
<script src="/resources/js/jalaliCalender/jalali.js"></script>
<script src="/resources/js/jalaliCalender/calendar.js"></script>
<script src="/resources/js/jalaliCalender/calendar-setup.js"></script>
<script src="/resources/js/jalaliCalender/lang/calendar-fa.js"></script>
<script src="/resources/js/formCheckCustome.js"></script>
<script type="text/javascript" src="/resources/js/clone-form-td.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="/resources/js/html5shiv.js"></script>
<script src="/resources/js/respond.min.js"></script>
<![endif]-->
</body>
</html>