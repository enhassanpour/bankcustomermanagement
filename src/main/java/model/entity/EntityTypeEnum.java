package model.entity;

public enum EntityTypeEnum {
    Customer(1),
    LegalCustomer(2),
    Junction(3);
    private int value;
    EntityTypeEnum(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }


}
