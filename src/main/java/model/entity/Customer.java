package model.entity;

/*sql command for create table
    CREATE TABLE customer(
        id INT AUTO_INCREMENT PRIMARY KEY,
        nationalCod VARCHAR(100) NOT NULL UNIQUE,
        NAME VARCHAR(100) NOT NULL,
        family VARCHAR(100) NOT NULL,
        fatherName VARCHAR(100) NOT NULL,
        birthday VARCHAR(100),
 type INT
    )
 */
public class Customer {
    private int id;
    private String name;
    private String family;
    private String fatherName;
    private String birthDay;
    private String nationalCod;
    private EntityTypeEnum entityTypeEnum;

    public EntityTypeEnum getEntityTypeEnum() {
        return entityTypeEnum;
    }

    public Customer setEntityTypeEnum(EntityTypeEnum entityTypeEnum) {
        this.entityTypeEnum = entityTypeEnum;
        return this;
    }

    public int getId() {
        return id;
    }

    public Customer setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Customer setName(String name) {
        this.name = name;
        return this;
    }

    public String getFamily() {
        return family;
    }

    public Customer setFamily(String family) {
        this.family = family;
        return this;
    }

    public String getFatherName() {
        return fatherName;
    }

    public Customer setFatherName(String fatherName) {
        this.fatherName = fatherName;
        return this;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public Customer setBirthDay(String birthDay) {
        this.birthDay = birthDay;
        return this;
    }


    public String getNationalCod() {
        return nationalCod;
    }

    public Customer setNationalCod(String nationalCod) {
        this.nationalCod = nationalCod;
        return this;
    }
}
