package model.entity;

import javax.persistence.*;

@Entity(name = "conditionFacilities")
@Table(name = "ConditionFacilities")
public class ConditionFacilities {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "NUMBER")
    private int id;
    @Column(name = "name", columnDefinition = "NVARCHAR2(200)")
    private String name;
    @Column(name = "minimumTime", columnDefinition = "NUMBER")
    private int minimumTime;
    @Column(name = "maximumTime", columnDefinition = "NUMBER")
    private int maximumTime;
    @Column(name = "minimumAmount", columnDefinition = "NUMBER")
    private int minimumAmount;
    @Column(name = "maximumAmount", columnDefinition = "NUMBER")
    private int maximumAmount;
    @Version
    private int version;

    public int getVersion() {
        return version;
    }

    public ConditionFacilities setVersion(int version) {
        this.version = version;
        return this;
    }

    public int getId() {
        return id;
    }

    public ConditionFacilities setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ConditionFacilities setName(String name) {
        this.name = name;
        return this;
    }

    public int getMinimumTime() {
        return minimumTime;
    }

    public ConditionFacilities setMinimumTime(int minimumTime) {
        this.minimumTime = minimumTime;
        return this;
    }

    public int getMaximumTime() {
        return maximumTime;
    }

    public ConditionFacilities setMaximumTime(int maximumTime) {
        this.maximumTime = maximumTime;
        return this;
    }

    public int getMinimumAmount() {
        return minimumAmount;
    }

    public ConditionFacilities setMinimumAmount(int minimumAmount) {
        this.minimumAmount = minimumAmount;
        return this;
    }

    public int getMaximumAmount() {
        return maximumAmount;
    }

    public ConditionFacilities setMaximumAmount(int maximumAmount) {
        this.maximumAmount = maximumAmount;
        return this;
    }
}
