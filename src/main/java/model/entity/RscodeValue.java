package model.entity;

public class RscodeValue {
    private Rscode rscode;
    private String value;
    private long cod;

    public RscodeValue(Rscode rscode, String value, long cod) {
        this.rscode = rscode;
        this.value = value;
        this.cod = cod;
    }

    public Rscode getRscode() {
        return rscode;
    }

    public RscodeValue setRscode(Rscode rscode) {
        this.rscode = rscode;
        return this;
    }

    public String getValue() {
        return value;
    }

    public RscodeValue setValue(String value) {
        this.value = value;
        return this;
    }

    public long getCod() {
        return cod;
    }

    public RscodeValue setCod(long cod) {
        this.cod = cod;
        return this;
    }

    @Override
    public String toString() {
        return "RscodeValue{" +
                "rscode=" + rscode +
                ", value='" + value + '\'' +
                ", cod=" + cod +
                '}';
    }
}
