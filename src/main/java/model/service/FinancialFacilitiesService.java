package model.service;

import model.entity.FinancialFacilities;
import model.repository.JpaRepositoryHibernate;

public class FinancialFacilitiesService {
    JpaRepositoryHibernate jpaRepositoryHibernate=new JpaRepositoryHibernate();
    public int save(FinancialFacilities financialFacilities)
    {
      return   jpaRepositoryHibernate.save(financialFacilities);
    }
}
