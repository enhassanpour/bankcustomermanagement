package model.service;

import model.entity.Customer;
import model.repository.JpaRepository;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class CustomerService extends CommonService {
    private JpaRepository jpaRepository = new JpaRepository();

    public int save(Customer customer) {
        return jpaRepository.save(customer);
    }

    public Customer findOne(String fildName, String fildValue) {
        List<Customer> customerList = jpaRepository.findOne(fildName, fildValue, Customer.class);
        try {

            if (customerList != null) {
                return new Customer().setName(customerList.get(0).getName()).setId(customerList.get(0).getId()).setFamily(customerList.get(0).getFamily()).setBirthDay(customerList.get(0).getBirthDay()).setFatherName(customerList.get(0).getFatherName()).setNationalCod(customerList.get(0).getNationalCod());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Customer> findAll() {
        return jpaRepository.findAll(Customer.class);

    }

    public int update(Customer customer) {
        return jpaRepository.update(customer, Customer.class);
    }

    public int delete(int id) {
        return jpaRepository.delete(Customer.class, id);
    }

    public List<Customer> findCondition(String condition) {
        return jpaRepository.findCondition(Customer.class, condition);

    }


}
