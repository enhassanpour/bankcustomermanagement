package model.service;

import model.entity.Junction;
import model.repository.JpaRepository;

public class JunctionService extends CommonService {
    public int delete(int legalCustomerID, int customerID) {
        String conditon = conditionMaker("", "legalCustomerID = " + legalCustomerID, "and");
        conditon = conditionMaker(conditon, "customerID = " + customerID, "and");
        JpaRepository jpaRepository = new JpaRepository();
        return (jpaRepository.deleteByCondition(Junction.class, conditon));
    }

    public int save(int legalCustomerID, int customerID) {
        Junction junction = new Junction();
        junction.setCustomerID(customerID);
        junction.setLegalCustomerID(legalCustomerID);
        JpaRepository jpaRepository = new JpaRepository();
        return jpaRepository.save(junction);
    }
}
