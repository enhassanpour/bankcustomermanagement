package model.service;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class CommonService {
    public String conditionMaker(String conditionStr, String parameterValue, String condition) {
        if (conditionStr.isEmpty())
            return " where " + conditionStr + parameterValue;
        else
            return conditionStr += " " + condition + " " + parameterValue;

    }

    public HttpServletRequest getActivePanel(HttpServletRequest req, String active) {

        if (req.getParameterMap().containsKey("active")) {
            active = req.getParameter("active");
        }
        switch (active) {
            case "activeRegisterPanel":
                req.setAttribute("activeRegisterLI", "active");
                req.setAttribute("activeRegisterPanel", "active in");
                break;
            case "activeBasePanel":
                req.setAttribute("activeBasePanel", "active in");
                req.setAttribute("activeBaseLI", "active");
                break;
            case "activeUpdatePanel":
                req.setAttribute("activeUpdateLI", "active");
                req.setAttribute("activeUpdatePanel", "active in");
                break;
            case "activeSearchPanel":
                req.setAttribute("activeSearchLI", "active");
                req.setAttribute("activeSearchPanel", "active in");
                break;
        }
        return req;
    }
}
