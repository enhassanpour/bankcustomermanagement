package model.service;

import model.entity.Rscode;
import model.entity.RscodeValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RscodeService {
    private static RscodeService rscodeService = new RscodeService();

    public RscodeService() {
    }

    public static List<RscodeValue> rscodeValueList = new ArrayList<>();

    public static RscodeService getInstance() {
        rscodeValueList = getRscodeValues("");
        return rscodeService;
    }

    public static RscodeValue getRscodeValue(Rscode rscode) {
        return rscodeValueList.stream().filter(p -> p.getRscode() == rscode).findFirst().get();
    }

    public static List<RscodeValue> getRscodeValues(String contextPath) {
        try {

            File file = new File(contextPath+"\\resources\\rsCod.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("error");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String descriptionRscode = element.getElementsByTagName("description").item(0).getTextContent();
                    Rscode rscode = Rscode.valueOf(element.getElementsByTagName("name").item(0).getTextContent());
                    Long codRscode = Long.valueOf(element.getElementsByTagName("cod").item(0).getTextContent());
                    RscodeValue rscodeValue = new RscodeValue(rscode, descriptionRscode, codRscode);
                    rscodeValueList.add(rscodeValue);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rscodeValueList;
    }
}
