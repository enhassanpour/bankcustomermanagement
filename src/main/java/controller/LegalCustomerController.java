package controller;

import com.google.gson.Gson;
import model.entity.Customer;
import model.entity.EntityTypeEnum;
import model.entity.LegalCustomer;
import model.entity.Rscode;
import model.service.CustomerService;
import model.service.LegalCustomerService;
import model.service.RscodeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class LegalCustomerController extends HttpServlet {
    private LegalCustomerService legalCustomerService = new LegalCustomerService();
    private int responseCod;
    private RscodeService rscodeService = new RscodeService();

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");

        if (req.getParameterMap().containsKey("active")) {
            req = legalCustomerService.getActivePanel(req, req.getParameter("active"));
        }

        switch (action) {
            case "save":
                req = save(req);
                req.getRequestDispatcher("/legalCustomer/legalCustomerService.do?action=findAll").forward(req, res);
            case "findAll":
                req = findAll(req);
                req.getRequestDispatcher("../legalCustomer.jsp").forward(req, res);
                break;
            case "update":
                req = update(req);
                req.getRequestDispatcher("/legalCustomer/legalCustomerService.do?action=findAll").forward(req, res);
                break;
            case "delete":
                req = delete(req);
                req.getRequestDispatcher("/legalCustomer/legalCustomerService.do?action=findAll").forward(req, res);
                break;
            case "search":
                res.setContentType("application/json");
                res.setCharacterEncoding("UTF-8");
                res.getWriter().write(new Gson().toJson(search(req)));
                break;
        }
    }

    public static LegalCustomer getLegalCustomerFromRequest(HttpServletRequest request) {

        LegalCustomer legalCustomer = new LegalCustomer();
        try {
            legalCustomer.setCompanyName(URLDecoder.decode(request.getParameter("companyName"), "UTF-8"));
            legalCustomer.setEconomicCod(URLDecoder.decode(request.getParameter("economicCod"), "UTF-8"));
            legalCustomer.setRegisterDate(URLDecoder.decode(request.getParameter("registerDate"), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (request.getParameterMap().containsKey("id")) {
            legalCustomer.setId(Integer.valueOf(request.getParameter("id")));
        }

        if (request.getParameterMap().containsKey("name")) {
            Customer customer = new Customer();
            CustomerController customerController = new CustomerController();
            customer = CustomerController.getPersonFromRequest(request);
            customer.setEntityTypeEnum(EntityTypeEnum.LegalCustomer);
            legalCustomer.setCustomer(customer);
        }

        return legalCustomer;
    }

    public HttpServletRequest save(HttpServletRequest req) {
        responseCod = legalCustomerService.save(getLegalCustomerFromRequest(req));
        String responseText = "";
        if (responseCod != 0) {
            responseText = rscodeService.getRscodeValue(Rscode.logLegalCustomerRegister).getValue() + responseCod;
        } else {
            responseText = rscodeService.getRscodeValue(Rscode.ERROR).getValue() ;

        }
        req.setAttribute("reponseRegister", responseText);
        req = legalCustomerService.getActivePanel(req, "activeRegisterPanel");
        return req;
    }

    public HttpServletRequest update(HttpServletRequest req) {
        responseCod = legalCustomerService.update(getLegalCustomerFromRequest(req));
        String responseText = "";

        if (responseCod != 0) {
            responseText = rscodeService.getRscodeValue(Rscode.done).getValue();
        } else {
            responseText = rscodeService.getRscodeValue(Rscode.ERROR).getValue();
        }
        req.setAttribute("responseUpdate", responseText);
        req = legalCustomerService.getActivePanel(req, "activeUpdatePanel");
        return req;
    }

    public HttpServletRequest delete(HttpServletRequest req) {
        int legalCustomerID = Integer.valueOf(req.getParameter("legalCustomerID"));
        int customerID = Integer.valueOf(req.getParameter("customerID"));
        int responseCodLegalCustomerID = legalCustomerService.delete(legalCustomerID, customerID);

        if (responseCodLegalCustomerID != 0) {
            req.setAttribute("responseUpdate", rscodeService.getRscodeValue(Rscode.done).getValue());
        } else {
            req.setAttribute("responseUpdate", rscodeService.getRscodeValue(Rscode.ERROR).getValue());
        }

        req = legalCustomerService.getActivePanel(req, "activeUpdatePanel");
        return req;
    }

    public List<LegalCustomer> search(HttpServletRequest req) {
        LegalCustomer legalCustomer = getLegalCustomerFromRequest(req);
        String condition = "";

        if (!legalCustomer.getCompanyName().isEmpty())
            condition = legalCustomerService.conditionMaker(condition, "companyName like '%" + legalCustomer.getCompanyName() + "%'", req.getParameter("conditionCompanyName"));

        if (!legalCustomer.getEconomicCod().isEmpty())
            condition = legalCustomerService.conditionMaker(condition, "economicCod like '%" + legalCustomer.getEconomicCod() + "%'", req.getParameter("conditionEconomicCod"));

        if (!legalCustomer.getRegisterDate().isEmpty())
            condition = legalCustomerService.conditionMaker(condition, "registerDate like '%" + legalCustomer.getRegisterDate() + "%'", req.getParameter("conditionRegisterDate"));

        req.setAttribute("listSearch", legalCustomerService.findCondition(condition));
        req = legalCustomerService.getActivePanel(req, "activeSearchPanel");
        return legalCustomerService.findCondition(condition);

    }

    public HttpServletRequest findAll(HttpServletRequest req) {
        List list = legalCustomerService.findAll();
        req.setAttribute("list", list);

        if (req.getAttribute("reponseRegister") == null) {
            req.setAttribute("reponseRegister", "");
        }

        if (req.getAttribute("responseUpdate") == null) {
            req.setAttribute("responseUpdate", "");
        }

        if (req.getAttribute("responseSearch") == null) {
            req.setAttribute("responseSearch", "");
        }

        return req;
    }
}
