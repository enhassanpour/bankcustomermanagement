package controller;

import com.google.gson.Gson;
import model.entity.Customer;
import model.entity.EntityTypeEnum;
import model.entity.Rscode;
import model.service.CustomerService;
import model.service.RscodeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class CustomerController extends HttpServlet {
    private int responseCod;
    private CustomerService customerService = new CustomerService();
    private RscodeService rscodeService = new RscodeService();

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");

        if (req.getParameterMap().containsKey("active")) {
            req = customerService.getActivePanel(req, req.getParameter("active"));
        }

        switch (action) {
            case "save":
                save(req).getRequestDispatcher("/customer/customerService.do?action=findAll").forward(req, res);
                break;
            case "findAll":
                req = findAll(req);
                if (req.getParameterMap().containsKey("url"))
                    req.getRequestDispatcher(req.getParameter("url")).forward(req, res);
                else
                    req.getRequestDispatcher("../customer.jsp").forward(req, res);
                break;
            case "update":
                req = update(req);
                req.getRequestDispatcher("/customer/customerService.do?action=findAll").forward(req, res);
                break;
            case "delete":
                delete(req);
                req.getRequestDispatcher("/customer/customerService.do?action=findAll").forward(req, res);
                break;
            case "search":
                search(req);
                req = customerService.getActivePanel(req, "activeSearchPanel");
                res.setContentType("application/json");
                res.setCharacterEncoding("UTF-8");
                res.getWriter().write(new Gson().toJson(search(req)));
                break;
        }


    }

    public HttpServletRequest save(HttpServletRequest req) {
        responseCod = customerService.save(getPersonFromRequest(req).setEntityTypeEnum(EntityTypeEnum.Customer));
        String responseText = "";
        if (responseCod != 1) {
            responseText = rscodeService.getRscodeValue(Rscode.CustomerRegister).getValue() + responseCod;
        } else {
            responseText = rscodeService.getRscodeValue(Rscode.ERROR).getValue() ;
        }
        req.setAttribute("reponseRegister", responseText);
        req = customerService.getActivePanel(req, "activeRegisterPanel");
        return req;
    }

    public HttpServletRequest findAll(HttpServletRequest req) {
        req.setAttribute("list", customerService.findAll());
        if (req.getAttribute("reponseRegister") == null) {
            req.setAttribute("reponseRegister", "");
        }
        if (req.getAttribute("responseUpdate") == null) {
            req.setAttribute("responseUpdate", "");
        }
        if (req.getAttribute("responseSearch") == null) {
            req.setAttribute("responseSearch", "");
        }
        return req;
    }

    public HttpServletRequest update(HttpServletRequest req) {
        responseCod = customerService.update(getPersonFromRequest(req));
        if (responseCod != 0) {
            req.setAttribute("responseUpdate", rscodeService.getRscodeValue(Rscode.done).getValue());
        }
        req = customerService.getActivePanel(req, "activeUpdatePanel");
        return req;
    }

    public HttpServletRequest delete(HttpServletRequest req) {
        int id = Integer.valueOf(req.getParameter("id"));
        responseCod = customerService.delete(id);
        if (responseCod != 0) {
            req.setAttribute("responseUpdate", rscodeService.getRscodeValue(Rscode.done).getValue());
        }
        req = customerService.getActivePanel(req, "activeUpdatePanel");
        return req;
    }

    public List<Customer> search(HttpServletRequest req) {
        Customer customer = getPersonFromRequest(req);
        String condition = "";
        if (!customer.getName().isEmpty())
            condition = customerService.conditionMaker(condition, "name like '%" + customer.getName() + "%'", req.getParameter("conditionName"));
        if (!customer.getBirthDay().isEmpty())
            condition = customerService.conditionMaker(condition, "birthDay like '%" + customer.getBirthDay() + "%'", req.getParameter("conditionBirthDay"));
        if (!customer.getFamily().isEmpty())
            condition = customerService.conditionMaker(condition, "family like '%" + customer.getFamily() + "%'", req.getParameter("conditionFamily"));
        if (!customer.getFatherName().isEmpty())
            condition = customerService.conditionMaker(condition, "fatherName like '%" + customer.getFatherName() + "%'", req.getParameter("conditionFatherName"));
        if (!customer.getNationalCod().isEmpty())
            condition = customerService.conditionMaker(condition, "nationalCod like '%" + customer.getNationalCod() + "%'", req.getParameter("conditionNationalCod"));
        condition = customerService.conditionMaker(condition, "type = " + EntityTypeEnum.Customer.getValue(), "and");

        return customerService.findCondition(condition);

    }
    public static Customer getPersonFromRequest(HttpServletRequest request) {
        Customer customer = new Customer();
        try {
            customer.setName(URLDecoder.decode(request.getParameter("name"), "UTF-8"));
            customer.setFamily(URLDecoder.decode(request.getParameter("family"), "UTF-8"));
            customer.setFatherName(URLDecoder.decode(request.getParameter("fatherName"), "UTF-8"));
            customer.setBirthDay(URLDecoder.decode(request.getParameter("birthDay"), "UTF-8"));
            customer.setNationalCod(URLDecoder.decode(request.getParameter("nationalCod"), "UTF-8"));
            if (request.getParameterMap().containsKey("id")) {
                customer.setId(Integer.valueOf(request.getParameter("id")));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return customer;
    }


    public HttpServletRequest getActivePanel(HttpServletRequest req, String active) {

        if (req.getParameterMap().containsKey("active")) {
            active = req.getParameter("active");
        }
        switch (active) {
            case "activeRegisterPanel":
                req.setAttribute("activeRegisterLI", "active");
                req.setAttribute("activeRegisterPanel", "active in");
                break;
            case "activeBasePanel":
                req.setAttribute("activeBasePanel", "active in");
                req.setAttribute("activeBaseLI", "active");
                break;
            case "activeUpdatePanel":
                req.setAttribute("activeUpdateLI", "active");
                req.setAttribute("activeUpdatePanel", "active in");
                break;
            case "activeSearchPanel":
                req.setAttribute("activeSearchLI", "active");
                req.setAttribute("activeSearchPanel", "active in");
                break;
        }
        return req;
    }
}
